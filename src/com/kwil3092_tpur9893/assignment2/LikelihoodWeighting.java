package com.kwil3092_tpur9893.assignment2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import com.kwil3092_tpur9893.assignment2.nodes.Sample;

/**
 * The LikelihoodWeighting class contains all the N samples for storage. It then
 * performs manipulation and processing of the data to congregate the repeated
 * samples into unique samples and normalise the congregated data in those samples.
 * @author Tomislav, Kerrod
 *
 */
public class LikelihoodWeighting {
	
	// All outcome combinations (data will merge into either of these 4 points)
	public static final String outcome[] = {"tttt", "fttt", "ttft", "ftft"};
	
	private Sample[] sample;
	private HashMap<String, BigDecimal> data = new HashMap<String, BigDecimal>();
	
	/**
	 * The LikelihoodWeighting default constructor absorbs all the N samples for storage, manipulation,
	 * and data processing.
	 * @param sample is the array of samples the program is to absorb for manipulation and processing
	 */
	public LikelihoodWeighting(Sample[] sample) {
		this.sample = sample;

		congregateData();
		normalise();
	}
	
	/**
	 * returns the data all the samples added together
	 * @return data in the form of a HashMap
	 */
	public HashMap<String, BigDecimal> getData() {
		return data;
	}
	
	/**
	 * returns the specific element from the data of all the samples added together
	 * @param outcome is the specific data to extract
	 * @return data element belonging to outcome
	 */
	public BigDecimal getData(String outcome) {
		return data.get(outcome);
	}
	
	/**
	 * normalises the data
	 */
	public void normalise() {
		// Normalises all the data added by dividing by the maximum weight
		BigDecimal max = maxWeighting();
		for (int i = 0; i < outcome.length; i++) {
			data.put(outcome[i], data.get(outcome[i]).divide(max, 3, RoundingMode.HALF_EVEN));
		}
	}
	
	/**
	 * places and adds all the data into the unique outcomes
	 */
	private void congregateData() {
		// Initialise data
		for (int i = 0; i < outcome.length; i++) {
			data.put(outcome[i], new BigDecimal(0.0));
		}
		
		// Add data 
		for (int i = 0; i < sample.length; i++) {
			String s = sample[i].getSample();
			data.put(s, data.get(s).add(sample[i].getProbability()));
		}
	}
	
	/**
	 * Calculates the combined weight from all the samples so we can normalise the data
	 * @return maximum weight
	 */
	private BigDecimal maxWeighting() {
		BigDecimal max = new BigDecimal(0.0);
		for (int i = 0; i < sample.length; i++) {
			max = max.add(sample[i].getProbability());
		}
		return max;
	}

}
