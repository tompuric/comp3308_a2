package com.kwil3092_tpur9893.assignment2.statistics;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.kwil3092_tpur9893.assignment2.Algorithm;
import com.kwil3092_tpur9893.assignment2.LikelihoodWeighting;
import com.kwil3092_tpur9893.assignment2.nodes.Sample;

/**
 * Stats is the Statistics class used for gathering, storing, calculating, and printing
 * data from the algorithm and providing us with information
 * @author Tomislav, Kerrod
 *
 */
public class Stats {
	
	private int M;
	private List<String> sample = new ArrayList<String>();
	
	private HashMap<String, BigDecimal> means = new HashMap<String, BigDecimal>();
	private HashMap<String, BigDecimal> variances = new HashMap<String, BigDecimal>();
	
	private int count = 1;
	
	String outcome;
	BigDecimal mean;
	BigDecimal variance;
	BigDecimal N[][] = new BigDecimal[4][4];
	
	/**
	 * Initialises the Stats class by providing the variable M, initalising
	 * variables and setting all values to zero
	 * @param M is the total number of times the program will run for. Determines
	 * how much to divide the numbers to determine their averages
	 */
	public Stats(int M) {
		this.M = M;
		String outcome[] = LikelihoodWeighting.outcome;
		for (int i = 0; i < outcome.length; i++) {
			means.put(outcome[i], BigDecimal.valueOf(0.0));
			variances.put(outcome[i], BigDecimal.valueOf(0.0));
		}
		for (int i = 0; i < N.length; i++) {
			for (int j = 0; j < N.length; j++) {
				N[i][j] = BigDecimal.valueOf(0.0);
			}
		}
	}
	
	/**
	 * Adds the LikelihoodWeighting calculations to the statistics
	 * @param samples to add and calculate
	 */
	public synchronized void addLW(Sample samples[]) {
		sample.add("\nRun: " + count);
		count++;
		sample.add("N\t\t tttt\tfttt\tttft\tftft");
		for (int i = 0; i < samples.length; i++) {
			outcome = LikelihoodWeighting.outcome[i];
			sample.add("N="+Algorithm.N[i]+":\t " + samples[i].getResults());
			for (int j = 0; j < samples.length; j++) {
				N[i][j] = N[i][j].add(samples[j].getData().get(outcome));
			}
		}
		sample.add("");
	}
	
	/**
	 * Adds the Mean and Variance calculations to the statistics
	 * @param samples to add and calculate
	 */
	public synchronized void addMV(Sample samples[]) {
		for (int i = 0; i < samples.length; i++) {
			outcome = samples[i].getSample();
			mean = samples[i].getMean();
			variance = samples[i].getVariance();
			
			sample.add("outcome: " + outcome + ",\tMean: " +  mean.toString() + ",\tVariance: " + variance.toString());
			means.put(outcome, means.get(outcome).add(mean));
			variances.put(outcome, variances.get(outcome).add(variance));	
		}
	}
	
	/**
	 * Prints the Results in the console. Samples were
	 * evaded to only show was is necessary.
	 */
	public void printResults() {
		System.out.println("Results:");
		
		/*
		for (int i = 0; i < sample.size(); i++) {
			System.out.println(sample.get(i));
		}*/
		
		System.out.println("\nResults over " + M + " runs.");
		
		String outcome[] = LikelihoodWeighting.outcome;
		
		System.out.println("\nN\t tttt\tfttt\tttft\tftft");
		for (int i = 0; i < outcome.length; i++) {
			System.out.print("N="+Algorithm.N[i]+":\t ");
			for (int j = 0; j < outcome.length; j++) {
				System.out.print(N[j][i].divide(BigDecimal.valueOf(M), 3, RoundingMode.HALF_UP) + "\t");
			}
			System.out.println();
		}
		
		System.out.println();
		
		for (int i = 0; i < outcome.length; i++) {
			System.out.print("outcome: " + outcome[i] + "\tmean: " + means.get(outcome[i]).divide(BigDecimal.valueOf(M), 5, RoundingMode.HALF_EVEN));
			System.out.println("\tvariance: " + variances.get(outcome[i]).divide(BigDecimal.valueOf(M), 5, RoundingMode.HALF_EVEN));
		}
		
	}
	
	/**
	 * Prints the Results to two separate text files. Samples are
	 * printed to the Data.txt file while the main results over the
	 * M runs are printed in the Results.txt file
	 * @throws FileNotFoundException
	 */
	public void printToText() throws FileNotFoundException {
		PrintWriter out = new PrintWriter("Data.txt");
		out.write("Results:\n");
		
		for (int i = 0; i < sample.size(); i++) {
			out.write(sample.get(i) + "\n");
		}
		
		out.flush();
		out.close();

		out = new PrintWriter("Results.txt");		
		
		out.write("\nResults over " + M + " runs.\n");
		
		String outcome[] = LikelihoodWeighting.outcome;
		
		out.write("\nN\t\ttttt\tfttt\tttft\tftft");
		for (int i = 0; i < outcome.length; i++) {
			out.write("\nN="+Algorithm.N[i]+":\t ");
			for (int j = 0; j < outcome.length; j++) {
				out.write(N[j][i].divide(BigDecimal.valueOf(M), 3, RoundingMode.HALF_UP) + "\t");
			}
		}
		
		out.write("\n\n");
		
		for (int i = 0; i < outcome.length; i++) {
			out.write("outcome: " + outcome[i] + "\tmean: " + means.get(outcome[i]).divide(BigDecimal.valueOf(M), 3, RoundingMode.HALF_UP));
			out.write("\tvariance: " + variances.get(outcome[i]).divide(BigDecimal.valueOf(M), 3, RoundingMode.HALF_UP) + "\n");
		}
		
		out.flush();
		out.close();
	}

}
