package com.kwil3092_tpur9893.assignment2;

/**
 * Contains the main method for the program. Calls the Assignment2 class
 * @author Tomislav, Kerrod
 *
 */
public class Assignment2Main {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Assignment2 a2 = new Assignment2();
	}

}
