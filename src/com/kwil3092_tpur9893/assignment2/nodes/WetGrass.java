package com.kwil3092_tpur9893.assignment2.nodes;

import java.util.List;

/**
 * WetGrass node
 * @author Tomislav, Kerrod
 *
 */
public class WetGrass extends Node {
	
	/**
	 * Default Constructor sets the name of the Node to WetGrass
	 * and initialises its CPT.
	 */
	public WetGrass() {
		name = getClass().getSimpleName();
		probability.put("tt", 0.99);
		probability.put("tf", 0.90);
		probability.put("ft", 0.90);
		probability.put("ff", 0.00);
	}
	
	/**
	 * Initialises WetGrass
	 */
	public void init() {
		super.init();
	}
	
	/**
	 * samples the node, determining whether the node selected is true or false
	 * based on its dependencies
	 */
	public void sample() {
		List<Node> d = dependency;
		Node parentSprinkler = d.get(0);
		Node parentRain = d.get(1);
		sampleProbability = probability.get(parentSprinkler.getSelected() +
								   parentRain.getSelected());
		weight = sampleProbability;
		sampleSelected =  "t";
	}

}
