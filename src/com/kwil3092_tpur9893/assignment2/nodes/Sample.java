package com.kwil3092_tpur9893.assignment2.nodes;

import java.math.BigDecimal;
import java.util.HashMap;

import com.kwil3092_tpur9893.assignment2.LikelihoodWeighting;

/**
 * The Sample class contains all the data stored and gathered after a single
 * sample pass. It's also used as storage for multiple samples combined in one
 * in order to determine mean and variance.
 * @author Tomislav, Kerrod
 *
 */
public class Sample {
	
	private String distribution;
	private BigDecimal weightedProbability;
	private BigDecimal mean;
	private BigDecimal variance;
	private HashMap<String, BigDecimal> data;
	
	/**
	 * Initialises the sample with given data
	 * @param data
	 */
	public Sample(HashMap<String, BigDecimal> data) {
		this.data = data;
	}
	
	/**
	 * Initialises the sample with a distribution and its weighted probability
	 * @param distribution is sample of nodes taken. E.g. "TTFT" where Cloudy=T,
	 * Sprinkler=T, Rain=F, WetGrass=T
	 * @param weightedProbability is the weighted probability gathered from that
	 * specific single sample
	 */
	public Sample(String distribution, BigDecimal weightedProbability) {
		this.distribution = distribution;
		this.weightedProbability = weightedProbability;
	}
	
	/**
	 * Initialises the sample with a given distribution, mean, and variance. Acts
	 * as a sample of multiple samples.
	 * @param distribution is sample of nodes taken. E.g. "TTFT" where Cloudy=T,
	 * Sprinkler=T, Rain=F, WetGrass=T
	 * @param mean is the mean of this sample
	 * @param variance is the variance of this sample
	 */
	public Sample(String distribution, BigDecimal mean, BigDecimal variance) {
		this.distribution = distribution;
		this.mean = mean;
		this.variance = variance;
	}
	
	/** 
	 * returns the distribution of the sample
	 * @return null if distribution has not been set, otherwise the samples distribution. E.g. "ftft".
	 */
	public String getSample() {
		return distribution;
	}
	
	/**
	 * Returns the weighted probability of the sample
	 * @return returns the weightedProbability of the Sample, if there's no weight, 0 will be returned
	 */
	public BigDecimal getProbability() {
		return weightedProbability;
	}
	
	/**
	 * returns the mean of the sample of samples
	 * @return mean
	 */
	public BigDecimal getMean() {
		return mean;
	}
	
	/**
	 * returns the variance of the sample of samples
	 * @return variance
	 */
	public BigDecimal getVariance() {
		return variance;
	}
	
	/**
	 * Returns the data in the sample of samples
	 * @return data
	 */
	public HashMap<String, BigDecimal> getData() {
		return data;
	}
	
	/**
	 * Gets the results of the sample of samples
	 * @return result
	 */
	public String getResults() {
		String result = "";
		String outcome[] = LikelihoodWeighting.outcome;
		for (int i = 0; i < data.size(); i++) {
			result += (data.get(outcome[i]) + "\t");
		}
		return result;
	}

}
