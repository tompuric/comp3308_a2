package com.kwil3092_tpur9893.assignment2.nodes;

import java.util.List;

/**
 * Rain Node
 * @author Tomislav, Kerrod
 *
 */
public class Rain extends Node {
	public final double CLOUDY_TRUE = 0.5;
	public final double NON_CLOUDY_FALSE = 1.0;
	
	/**
	 * Default Constructor sets the name of the Node to Rain
	 * and initialises its CPT.
	 */
	public Rain() {
		name = getClass().getSimpleName();
		probability.put("t", 0.8);
		probability.put("f", 0.2);
	}
	
	/**
	 * initialises rain
	 */
	public void init() {
		super.init();
	}
	
	/**
	 * samples the node, determining whether the node selected is true or false
	 * based on its dependencies
	 */
	public void sample() {
		List<Node> d = dependency;
		Node parent = d.get(0);
		sampleProbability = probability.get(parent.getSelected());
		sampleSelected = seed < sampleProbability ? "t" : "f";
		if (sampleSelected.equals("f")) sampleProbability = probability.get("f");
	}

}
