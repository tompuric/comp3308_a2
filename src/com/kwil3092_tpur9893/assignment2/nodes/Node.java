package com.kwil3092_tpur9893.assignment2.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * The Node class is the base of all the nodes the program will sample
 * @author Tomislav, Kerrod
 *
 */
public class Node {
	protected Random random = new Random();
	protected String sampleSelected;
	protected double sampleProbability;
	protected double seed;
	protected double weight;
	protected String name;
	protected List<Node> dependency = new ArrayList<Node>();
	public HashMap<String, Double> probability = new HashMap<String, Double>();
	
	/**
	 * Initialises all the values
	 */
	public void init() {
		seed = random.nextDouble();
		weight = 1.0;
		sampleSelected = "";
		sampleProbability = 0.0;
	}
	
	public void tick() {
		
	}
	
	public void sample() {
	}
	
	/**
	 * returns the sample that was Selected (True or false)
	 * @return selected sample, "t" / "f"
	 */
	public String getSelected() {
		return sampleSelected;
	}
	
	/**
	 * returns the probability of the selected sample
	 * @return sample probability
	 */
	public double getProbability() {
		return sampleProbability;
	}
	
	/**
	 * Gets the weight of the node. If it's not given, the weight will be 1.
	 * @return sample weight
	 */
	public double getWeight() {
		return weight;
	}
	
	
	/**
	 * prints the Conditional Probability Table;
	 */
	public void printCPT() {
		
	}
	
	/**
	 * Adds a dependency to the node
	 * @param node to add (node becomes father)
	 */
	public void addDependency(Node node) {
		dependency.add(node);
	}
	
	/**
	 * Gets the list of father nodes
	 * @return list of dependency nodes
	 */
	public List<Node> getDependency() {
		return dependency;
	}
	
	/**
	 * Prints the dependency of the node
	 * @return String to print
	 */
	public String printDependency() {
		String print = "P(" + getName();
		if (dependency.size() != 0) print += " | ";
		for (int i = 0; i < dependency.size(); i++) {
			print += (dependency.get(i).getName());
			if (i != dependency.size() - 1) print += ", ";
		}
		print += ")";
		return print;
	}
	
	/**
	 * Gets the name of the Node
	 * @return name of Node
	 */
	public String getName() {
		return name;
	}
}
