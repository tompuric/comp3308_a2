package com.kwil3092_tpur9893.assignment2.nodes;

/**
 * Cloudy Node
 * @author Tomislav, Kerrod
 *
 */
public class Cloudy extends Node {
	
	public final double TRUE = 0.5;
	
	/**
	 * Default Constructor sets the name of the Node to Cloudy
	 */
	public Cloudy() {
		name = getClass().getSimpleName();
	}
	
	/**
	 * initialises Cloudy
	 */
	public void init() {
		super.init();
	}
	
	/**
	 * samples the node, determining whether the node selected is true or false
	 */
	public void sample() {
		sampleProbability = TRUE;
		sampleSelected = seed < TRUE ? "t" : "f";
	}
}
