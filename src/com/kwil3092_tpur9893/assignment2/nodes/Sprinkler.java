package com.kwil3092_tpur9893.assignment2.nodes;

import java.util.List;

/**
 * Sprinkler Node
 * @author Tomislav, Kerrod
 *
 */
public class Sprinkler extends Node {
	
	/**
	 * Default Constructor sets the name of the Node to Sprinkler
	 * and initialises its CPT.
	 */
	public Sprinkler() {
		name = getClass().getSimpleName();
		probability.put("t", 0.1);
		probability.put("f", 0.5);
	}
	
	/**
	 * initialises sprinkler
	 */
	public void init() {
		super.init();
	}
	
	/**
	 * samples the node, determining whether the node selected is true or false
	 * based on its dependencies
	 */
	public void sample() {
		List<Node> d = dependency;
		Node parent = d.get(0);
		weight = probability.get(parent.getSelected());
		sampleProbability = weight;
		sampleSelected =  "t";
	}

}
