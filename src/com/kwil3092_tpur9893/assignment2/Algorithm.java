package com.kwil3092_tpur9893.assignment2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.kwil3092_tpur9893.assignment2.nodes.Cloudy;
import com.kwil3092_tpur9893.assignment2.nodes.Node;
import com.kwil3092_tpur9893.assignment2.nodes.Rain;
import com.kwil3092_tpur9893.assignment2.nodes.Sample;
import com.kwil3092_tpur9893.assignment2.nodes.Sprinkler;
import com.kwil3092_tpur9893.assignment2.nodes.WetGrass;
import com.kwil3092_tpur9893.assignment2.statistics.Stats;

/**
 * Performs the Likelihood-Weighting Algorithm on the Cloudy-Sprinkler-Rain-WetGrass
 * network where Sprinkler=True and WetGrass=true.
 * @author Tomislav, Kerrod
 *
 */
public class Algorithm implements Runnable {
	
	public static final int N[] = {10, 100, 1000, 5000};
	
	public static final int CLOUDY = 0;
	public static final int SPRINKLER = 1;
	public static final int RAIN = 2;
	public static final int WETGRASS = 3;
	
	public List<Node> nodes = new ArrayList<Node>();
	
	public Stats stats;
	private BigDecimal weight = new BigDecimal(1.0);
	
	/**
	 * The default constructor for Algorithm accepts a Statistic class reference so we can
	 * track the algorithms progress, calculations, and output. It calls the init() method.
	 * @param stats is given to Algorithm so that it can update the variable with new information
	 */
	public Algorithm(Stats stats) {
		this.stats = stats;
		init();
	}
	
	/**
	 * init() initialises the algorithm variables. It creates the nodes for the network and
	 * assigns dependencies accordingly. The nodes and dependencies can be changed and
	 * reordered here for extensibility.
	 */
	private void init() {
		// Add Nodes
		nodes.add(new Cloudy());
		nodes.add(new Sprinkler());
		nodes.add(new Rain());
		nodes.add(new WetGrass());
		
		// Add Dependencies
		nodes.get(SPRINKLER).addDependency(nodes.get(CLOUDY));
		nodes.get(RAIN).addDependency(nodes.get(CLOUDY));
		nodes.get(WETGRASS).addDependency(nodes.get(SPRINKLER));
		nodes.get(WETGRASS).addDependency(nodes.get(RAIN));
	}
	
	/**
	 * The runnable method run() is executed when the thread is started
	 */
	@Override
	public void run() {
		tick();
	}
	
	/**
	 * Ticks the algorithm by performing the likelihood-weighting algorithm
	 * N times and collects the data gathered from each process accordingly
	 */
	private void tick() {
		
		/*
		 * Calculate normalised Weighted Probability for each sample
		 */
		
		LikelihoodWeighting lw[] = new LikelihoodWeighting[N.length];
		Sample[] samplesCollected = new Sample[N.length];
		
		// Loop through N times gathering data from generated samples
		for (int j = 0; j < N.length; j++) {
			lw[j] = likelihoodWeighting(j);
			samplesCollected[j] = new Sample(lw[j].getData());			
		}
		
		// Add data gathered to statistics
		stats.addLW(samplesCollected);
		
		/*
		 *  Calculate Mean and Variance for each N[j]
		 */
		String[] outcome = LikelihoodWeighting.outcome;
		samplesCollected = new Sample[outcome.length];
		
		// Loop through all the individual outcomes (in this case 4)
		// from data gathered in generated samples previously
		for (int j = outcome.length - 1; j >= 0; j--) {
			BigDecimal mean = mean(outcome[j], lw);
			BigDecimal variance = variance(outcome[j], lw, mean);
			samplesCollected[j] = new Sample(outcome[j], mean, variance);
		}
		
		// Add data gathered to statistics
		stats.addMV(samplesCollected);
	}
	
	/**
	 * calculates the mean of the data collected
	 * @param outcome is sample taken. E.g. "TTFT" where Cloudy=T, Sprinkler=T, Rain=F, WetGrass=T
	 * @param lw is the LikelihoodWeighting class which contains the data needed
	 * @return the average
	 */
	private BigDecimal mean(String outcome, LikelihoodWeighting[] lw) {
		BigDecimal avg = new BigDecimal(0.0);
		for (int i = 0; i < 4; i++) {
			avg = avg.add(lw[i].getData(outcome));
		}
		return avg.divide(BigDecimal.valueOf(lw.length));
		
	}
	
	/**
	 * calculates the variance of the data collected
	 * @param outcome is sample taken. E.g. "TTFT" where Cloudy=T, Sprinkler=T, Rain=F, WetGrass=T
	 * @param lw is the LikelihoodWeighting class which contains the data needed
	 * @param mean is the already calculated mean of the data
	 * @return the variance
	 */
	private BigDecimal variance(String outcome, LikelihoodWeighting[] lw, BigDecimal mean) {
		
		BigDecimal avg = new BigDecimal(0.0);
		for (int i = 0; i < lw.length; i++) {
			avg = avg.add((lw[i].getData(outcome).subtract(mean)).pow(2));
		}
		return avg.divide(BigDecimal.valueOf(lw.length), 5, RoundingMode.HALF_EVEN);
		
	}
	
	/**
	 * Likelihood-Weighting algorithm as shown in the Lecture Slides (Week 11, page 42)
	 * @param i selects which N value to use. N contains the number of samples to take
	 * @return a LikelihoodWeighting class which absorbs all the N samples for storage,
	 * manipulation and data processing.
	 */
	private LikelihoodWeighting likelihoodWeighting(int i) {
		int totalSamples = N[i];
		Sample sample[] = new Sample[totalSamples];
		for (int j = 0; j < totalSamples; j++) {
			sample[j] = weightedSample();
		}
		return new LikelihoodWeighting(sample);
	}
	
	/**
	 * Weighted Sample algorithm as shown in the Lecture Slides (Week 11, page 42)
	 * @return the weighted sample
	 */
	private Sample weightedSample() {
		weight = BigDecimal.valueOf(1.0);
		// X will represent the sample nodes that were selected, E.g. TTFT.
		String X = "";
		for (int i = 0; i < nodes.size(); i++) {
			Node n = nodes.get(i);
			n.init();
			n.sample();
			X += n.getSelected();
			weight = weight.multiply(BigDecimal.valueOf(n.getWeight()));
		}
		return new Sample(X, weight);
	}

}
