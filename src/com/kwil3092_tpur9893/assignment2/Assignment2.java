package com.kwil3092_tpur9893.assignment2;

import java.io.FileNotFoundException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.kwil3092_tpur9893.assignment2.statistics.Stats;

/**
 * Assignment2 creates a pool of threads to perform our algorithm and
 * prints the results to file and screen once all the threads have
 * been executed.
 * @author Tomislav, Kerrod
 *
 */
public class Assignment2 {

	public static final int NTHREADS = 16;
	public static final int M = 1000; // FIX LATER
	private Stats stats = new Stats(M);
	private ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);
	
	/**
	 * Default Constructor. Creates all the threads and prints results.
	 */
	public Assignment2() {
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < M; i++) {
			Runnable worker = new Algorithm(stats);
			executor.execute(worker);
		}
		
		// Wait for all threads to join
		executor.shutdown();
		
		// If program takes longer than 30 seconds to compute. return results already gathered
		try {
			executor.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
		} finally {
			// Once all threads have joined, print results
			printResults();
		}
		
		// Display time taken
		System.out.println("\nTime taken: " + (System.currentTimeMillis() - startTime)/1000.0);
	}
	
	/**
	 * Prints the results gathered from the Statistics class
	 */
	public void printResults() {
		stats.printResults();
		try {
			stats.printToText();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
}
